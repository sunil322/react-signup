import validator from "validator";

function nameValidator(name,inputName) {
    let value = inputName.trim();
    if (validator.isEmpty(value)) {
        return `${name} must not be empty`;
    } else if (validator.isAlpha(value) === false) {
        return `${name} should not contain any number or special characters`;
    } else if (value.length < 3 || value.length > 30) {
        return `${name} length should be 3-30 characters`;
    } else {
        return "";
    }
}

function ageValidator(inputAge) {
    let age = inputAge.trim();
    if (validator.isEmpty(age)) {
        return "Age must not be empty";
    } else if (Number(age) < 18 || Number(age) > 65) {
        return "Age should be between 18-65";
    } else {
        return "";
    }
}

function genderValidator(gender) {
    if (validator.isEmpty(gender)) {
        return "Select a gender";
    } else {
        return "";
    }
}

function roleValidator(role) {
    if (validator.isEmpty(role)) {
        return "Select a role";
    } else {
        return "";
    }
}

function emailValidator(inputEmail) {
    let email = inputEmail.trim();
    if (validator.isEmpty(email)) {
        return `Email must not be empty`;
    } else if (validator.isEmail(email) === false) {
        return "Invalid email address";
    } else {
        return "";
    }
}

function passwordValidator(inputPassword) {
    let password = inputPassword.trim();
    if (validator.isEmpty(password)) {
        return "Password must not be empty";
    } else if (validator.isStrongPassword(password) === false) {
        if (password.length < 8) {
            return "Password length sould be at least 8 characters";
        } else {
            return "Password should contain at least one lowercase, uppercase, number, symbol";
        }
    } else {
        return "";
    }
}

function matchingPassword(password, repeatPassword) {
    if (password !== repeatPassword) {
        return "Password didn't match";
    } else {
        return "";
    }
}
function termsAndConditionValidator(checkedBoxValue) {
    if (checkedBoxValue === "checked") {
        return "";
    } else {
        return "Please accept terms and condition";
    }
}

export { nameValidator, ageValidator, genderValidator, roleValidator, emailValidator, passwordValidator, matchingPassword, termsAndConditionValidator};
