import React, { Component } from "react";
import { nameValidator, ageValidator, genderValidator, roleValidator, emailValidator, passwordValidator, matchingPassword, termsAndConditionValidator} from "../validators/validator";
import SucessPage from "./SucessPage";

class Signup extends Component {
    constructor(props) {
        super(props);

        this.FORM_STATUS = {
            VALIDATED: "validated",
            NOT_VALIDATED: "notValidated",
        };

        this.state = {
            firstName: "",
            lastName: "",
            age: "",
            gender: "",
            role: "",
            email: "",
            password: "",
            repeatPassword: "",
            terms: "",
            errors: {},
            formValidationStatus: this.FORM_STATUS.NOT_VALIDATED,
        };
    }

    handleInputEvent = (event) => {
        const { name, value } = event.target;

        this.setState((prevState) => {
            return {
                ...prevState,
                [name]: value,
            };
        });
    };

    handleSubmitEvent = (event) => {
        event.preventDefault();

        const {
            firstName,
            lastName,
            age,
            gender,
            role,
            email,
            password,
            repeatPassword,
            terms,
        } = this.state;
        let allErrors = {};

        const updateErrorFields = (name, message = "") => {
            if (message !== "") {
                allErrors[name] = message;
            } else {
            }
        };

        updateErrorFields("firstName", nameValidator("Firstname", firstName));
        updateErrorFields("lastName", nameValidator("Lastname", lastName));
        updateErrorFields("age", ageValidator(age));
        updateErrorFields("gender", genderValidator(gender));
        updateErrorFields("role", roleValidator(role));
        updateErrorFields("email", emailValidator(email));
        updateErrorFields("password", passwordValidator(password));
        updateErrorFields(
            "repeatPassword",
            matchingPassword(password, repeatPassword)
        );
        updateErrorFields("terms", termsAndConditionValidator(terms));

        if (Object.keys(allErrors).length === 0) {
            this.setState({
                formValidationStatus: this.FORM_STATUS.VALIDATED,
            });
        } else {
            this.setState({
                formValidationStatus: this.FORM_STATUS.NOT_VALIDATED,
                errors: allErrors,
            });
        }
    };

    render() {
        return (
            <>
                {this.state.formValidationStatus ===
                this.FORM_STATUS.VALIDATED ? (
                    <SucessPage />
                ) : (
                    <form
                        className="rounded py-4 px-5 mx-auto my-2 bg-white shadow col-xl-4 col-lg-6 col-md-8 col-sm-10"
                        onSubmit={this.handleSubmitEvent}
                    >
                        <legend className="text-center primary text-warning">
                            SIGNUP FORM
                        </legend>
                        <div className="mb-2">
                            <label htmlFor="firstName" className="form-label">
                                <i className="fa-solid fa-user"></i> First Name
                            </label>
                            <input
                                type="text"
                                className={
                                    this.state.errors.firstName
                                        ? "form-control border-danger"
                                        : "form-control"
                                }
                                id="firstName"
                                name="firstName"
                                onChange={this.handleInputEvent}
                                value={this.state.firstName}
                                placeholder="Enter First Name"
                            />
                            {this.state.errors.firstName ? (
                                <p className="text-danger p-0 m-0">
                                    {this.state.errors.firstName}
                                </p>
                            ) : (
                                <p className="invisible p-0 m-0">""</p>
                            )}
                        </div>
                        <div className="mb-2">
                            <label htmlFor="lastName" className="form-label">
                                <i className="fa-solid fa-user"></i> Last Name
                            </label>
                            <input
                                type="text"
                                className={
                                    this.state.errors.lastName
                                        ? "form-control border-danger"
                                        : "form-control"
                                }
                                id="lastName"
                                name="lastName"
                                value={this.state.lastName}
                                onChange={this.handleInputEvent}
                                placeholder="Enter Last Name"
                            />
                            {this.state.errors.lastName ? (
                                <p className="text-danger p-0 m-0">
                                    {this.state.errors.lastName}
                                </p>
                            ) : (
                                <p className="invisible p-0 m-0">""</p>
                            )}
                        </div>
                        <div className="mb-2">
                            <label htmlFor="age" className="form-label">
                                <i className="fa-solid fa-person"></i> Age
                            </label>
                            <input
                                type="number"
                                className={
                                    this.state.errors.age
                                        ? "form-control border-danger"
                                        : "form-control"
                                }
                                id="age"
                                name="age"
                                value={this.state.age}
                                min="0"
                                onChange={this.handleInputEvent}
                                placeholder="Enter age"
                            />
                            {this.state.errors.age ? (
                                <p className="text-danger p-0 m-0">
                                    {this.state.errors.age}
                                </p>
                            ) : (
                                <p className="invisible p-0 m-0">""</p>
                            )}
                        </div>
                        <div className="mb-2">
                            <label className="form-label">
                                <i className="fa-solid fa-venus"></i> Gender
                            </label>
                            <br className="invisible" />
                            <div className="form-check form-check-inline">
                                <input
                                    className="form-check-input"
                                    type="radio"
                                    name="gender"
                                    id="male"
                                    value="male"
                                    onChange={this.handleInputEvent}
                                />
                                <label
                                    className="form-check-label"
                                    htmlFor="male"
                                >
                                    Male
                                </label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input
                                    className="form-check-input"
                                    type="radio"
                                    name="gender"
                                    id="female"
                                    value="female"
                                    onChange={this.handleInputEvent}
                                />
                                <label
                                    className="form-check-label"
                                    htmlFor="female"
                                >
                                    Female
                                </label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input
                                    className="form-check-input"
                                    type="radio"
                                    name="gender"
                                    id="others"
                                    value="others"
                                    onChange={this.handleInputEvent}
                                />
                                <label
                                    className="form-check-label"
                                    htmlFor="others"
                                >
                                    Others
                                </label>
                            </div>
                            {this.state.errors.gender ? (
                                <p className="text-danger p-0 m-0">
                                    {this.state.errors.gender}
                                </p>
                            ) : (
                                <p className="invisible p-0 m-0">""</p>
                            )}
                        </div>
                        <div className="mb-2">
                            <label htmlFor="email" className="form-label">
                                <i className="fa-solid fa-user-tie"></i> Role
                            </label>
                            <select
                                className={
                                    this.state.errors.role
                                        ? "form-control border-danger"
                                        : "form-control"
                                }
                                name="role"
                                onChange={this.handleInputEvent}
                                value={this.state.role}
                            >
                                <option value="default">Select Role</option>
                                <option value="developer">Developer</option>
                                <option value="seniorDeveloper">
                                    Senior Developer
                                </option>
                                <option value="leadEngineer">
                                    Lead Engineer
                                </option>
                                <option value="cto">CTO</option>
                            </select>
                            {this.state.errors.role ? (
                                <p className="text-danger p-0 m-0">
                                    {this.state.errors.role}
                                </p>
                            ) : (
                                <p className="invisible p-0 m-0">""</p>
                            )}
                        </div>
                        <div className="mb-2">
                            <label htmlFor="email" className="form-label">
                                <i className="fa-solid fa-envelope"></i> Email
                                Address
                            </label>
                            <input
                                type="email"
                                className={
                                    this.state.errors.email
                                        ? "form-control border-danger"
                                        : "form-control"
                                }
                                id="email"
                                name="email"
                                onChange={this.handleInputEvent}
                                value={this.state.email}
                                placeholder="Enter email address"
                            />
                            {this.state.errors.email ? (
                                <p className="text-danger p-0 m-0">
                                    {this.state.errors.email}
                                </p>
                            ) : (
                                <p className="invisible p-0 m-0">""</p>
                            )}
                        </div>
                        <div className="mb-2">
                            <label htmlFor="password" className="form-label">
                                <i className="fa-solid fa-lock"></i> Password
                            </label>
                            <input
                                type="password"
                                className={
                                    this.state.errors.password
                                        ? "form-control border-danger"
                                        : "form-control"
                                }
                                id="password"
                                name="password"
                                onChange={this.handleInputEvent}
                                value={this.state.password}
                                placeholder="Enter password"
                            />
                            {this.state.errors.password ? (
                                <p className="text-danger p-0 m-0">
                                    {this.state.errors.password}
                                </p>
                            ) : (
                                <p className="invisible p-0 m-0">""</p>
                            )}
                        </div>
                        <div className="mb-2">
                            <label
                                htmlFor="repeatPassword"
                                className="form-label"
                            >
                                <i className="fa-solid fa-lock"></i> Repeat
                                Password
                            </label>
                            <input
                                type="password"
                                className={
                                    this.state.errors.repeatPassword
                                        ? "form-control border-danger"
                                        : "form-control"
                                }
                                id="repeatPassword"
                                name="repeatPassword"
                                onChange={this.handleInputEvent}
                                value={this.state.repeatPassword}
                                placeholder="Repeat Password"
                            />
                            {this.state.errors.repeatPassword ? (
                                <p className="text-danger p-0 m-0">
                                    {this.state.errors.repeatPassword}
                                </p>
                            ) : (
                                <p className="invisible p-0 m-0">""</p>
                            )}
                        </div>
                        <div className="mb-2 form-check">
                            <input
                                type="checkbox"
                                className="form-check-input"
                                id="terms"
                                name="terms"
                                onChange={this.handleInputEvent}
                                value={
                                    this.state.terms === "checked"
                                        ? ""
                                        : "checked"
                                }
                            />
                            <label className="form-check-label" htmlFor="terms">
                                I agree to terms and conditions
                            </label>
                            {this.state.errors.terms ? (
                                <p className="text-danger p-0 m-0">
                                    {this.state.errors.terms}
                                </p>
                            ) : (
                                <p className="invisible p-0 m-0">""</p>
                            )}
                        </div>
                        <div>
                            <button
                                type="submit"
                                className="btn btn-primary py-2 w-100"
                            >
                                SIGNUP
                            </button>
                        </div>
                    </form>
                )}
            </>
        );
    }
}

export default Signup;