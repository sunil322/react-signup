import React, { Component } from "react";

class SucessPage extends Component {
    render() {
        return (
            <div className="text-center mt-5">
                <h2 className="text-primary">
                    You have signed up successfully
                </h2>
            </div>
        );
    }
}

export default SucessPage;
